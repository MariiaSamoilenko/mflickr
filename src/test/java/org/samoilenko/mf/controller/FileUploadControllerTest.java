package org.samoilenko.mf.controller;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.samoilenko.mf.dao.ImageDao;
import org.samoilenko.mf.entity.ImageEntity;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Test class for {@link FileUploadController} class.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
public class FileUploadControllerTest {

    private ImageDao dummyImageDao;
    private FileUploadController controller;
    private MockMvc mvc;
    private MockMultipartFile file;
    private UploadUtil dummyUploadUtil;
    private List<ImageEntity> list;
    private ImageEntity imageEntity;

    @Before
    public void setUp() throws Exception {
        dummyImageDao = mock(ImageDao.class);
        when(dummyImageDao.uploadImage(anyObject())).thenReturn(11L);
        when(dummyImageDao.getImageById(anyLong())).thenReturn(imageEntity);


        ImageEntity imageEntity = new ImageEntity("Rainbow", "2014-12-12");
        imageEntity.setIdImage(2);
        ImageEntity imageEntity2 = new ImageEntity("MyCat", "2015-12-12");
        imageEntity2.setIdImage(1);
        list = new ArrayList(){{add(imageEntity);add(imageEntity2);}};

        dummyUploadUtil = mock(UploadUtil.class);
        controller = new FileUploadController();
        ReflectionTestUtils.setField(controller, "imageDao", dummyImageDao);
        ReflectionTestUtils.setField(controller, "uploadUtil", dummyUploadUtil);
        file = new MockMultipartFile("file", "file", null, "bar".getBytes());
        mvc = standaloneSetup(controller).build();


    }

    @Test
    public void testHandleFileUpload() throws Exception {

        when(dummyUploadUtil.isImage(anyObject())).thenReturn(true);
        when(dummyUploadUtil.imageToStore(anyObject(), anyLong())).thenReturn("11");
        when(dummyUploadUtil.verifyDate(anyString())).thenReturn("2012-12-12");
        when(dummyUploadUtil.writeFiles(anyObject(),
                anyLong(), anyObject())).thenReturn("11");

        mvc.perform(MockMvcRequestBuilders.fileUpload("/upload")
                .file(file)
                .param("date", "2012-12-12").
                        param("desc", "aboutcat")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().
                        string(equalTo("{\"imageId\":\"11\",\"message\":\"The image successfully uploaded\",\"status\":\"200\"}")));
    }

    @Test
    public void testGetAll() throws Exception {
        when(dummyImageDao.getAll()).thenReturn(list);
        mvc.perform(MockMvcRequestBuilders.get("/getAll").
                accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful()).
                andExpect(content().string(equalTo("[{\"dataImage\":\"2014-12-12\",\"imageDescription\":" +
                        "\"Rainbow\",\"idImageEntity\":2},{\"dataImage\":" +
                        "\"2015-12-12\",\"imageDescription\":\"MyCat\",\"idImageEntity\":1}]")));
    }
    @Ignore
    @Test
    public void testGetById() throws Exception {
        when(dummyImageDao.getImageById(2)).thenReturn(imageEntity);
        mvc.perform(MockMvcRequestBuilders.get("/2").
                accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(equalTo("{\"dataImage\":\"2014-12-12\"," +
                        "\"imageDescription\":\"Rainbow\",\"idImageEntity\":2}")));
    }

    @Test
    public void testHandleFileUploadDaoException() throws Exception {
        ImageDao dummyImageDaoTest = mock(ImageDao.class);
        doThrow(new RuntimeException()).when(dummyImageDaoTest).getAll();
        ReflectionTestUtils.setField(controller, "imageDao", dummyImageDaoTest);
        mvc.perform(MockMvcRequestBuilders.get("/getAll").
                accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is5xxServerError());

    }

    @Test
    public void testHandleFileUploadNotImage() throws Exception {
        MockMultipartFile fileTest = new MockMultipartFile("file.txt", this.getClass().getResourceAsStream("/file.txt"));
        UploadUtil dummyUploadUtil3 = mock(UploadUtil.class);
        when(dummyUploadUtil3.isImage(fileTest.getOriginalFilename())).thenCallRealMethod();
        ReflectionTestUtils.setField(controller, "uploadUtil", dummyUploadUtil3);
        mvc.perform(MockMvcRequestBuilders.fileUpload("/upload")
                .file(fileTest)
                .param("date", "2012-12-12").
                        param("desc", "about2")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    public void testGetByIdException() throws Exception {
        ImageDao dummyImageDaoTest2 = mock(ImageDao.class);

        //when(dummyImageDaoTest2.getImageById(anyLong())).thenCallRealMethod();
        doThrow(new RuntimeException()).when(dummyImageDaoTest2).getImageById(anyLong());
        ReflectionTestUtils.setField(controller, "imageDao", dummyImageDaoTest2);
        mvc.perform(MockMvcRequestBuilders.get("/100").
                accept(MediaType.APPLICATION_JSON)).
                andExpect(status().is5xxServerError());

    }
}

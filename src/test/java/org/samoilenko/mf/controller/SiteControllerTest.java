package org.samoilenko.mf.controller;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.mock.env.MockEnvironment;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;
/**
 * Test class for {@link SiteController} class.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@ComponentScan("org.samoilenko.mf.controller")
public class SiteControllerTest {

    private SiteController siteController;
    private MockMvc mvc;
    private MockEnvironment env;


    @Before
    public void setup(){
        siteController = new SiteController();
        env = new MockEnvironment();
        env.setProperty("image.url","http://localhost:8080/getAll");
        env.setProperty("image.description","static/description/");
        env.setProperty("image.view","static/images/");
        ReflectionTestUtils.setField(siteController, "env", env);
        mvc = standaloneSetup(siteController).build();

    }
    @Ignore
    @Test
    public void indexTest() throws Exception{
        this.mvc.perform(get("/")).andExpect(status().isOk());
    }
}

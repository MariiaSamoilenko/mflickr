package org.samoilenko.mf.dao;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.samoilenko.mf.configuration.JpaConfig;
import org.samoilenko.mf.domain.Image;
import org.samoilenko.mf.entity.ImageEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Test class for {@link ImageDaoImpl} class.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JpaConfigTest.class})
public class ImageDaoImplTest {

    @Autowired
   private ImageDao imageDao;
   private Image image;

    @Before
    public void SetUp() {
        image = new Image();
        image.setImageData("2013-1-12");
        image.setImageDescription("somefunnydescription");
    }

    @Test
    public void testUploadImage() throws Exception {
       assertEquals("The id should be equal, cause just one image uploaded",
               1, imageDao.uploadImage(image));

    }

    @Test
    public void testGetAll() throws Exception {
        assertNotNull("The list of images shouldn't be null", imageDao.getAll());
    }
    @Ignore
    @Test
    public void testGetImageById() throws Exception {
        ImageEntity em = new ImageEntity();
        em.setImageDescription(image.getImageDescription());
        em.setDateImage(image.getImageData());
        em.setIdImage(1);
       assertTrue("The images should be equals", em.equals(imageDao.getImageById(1)) );
    }
}
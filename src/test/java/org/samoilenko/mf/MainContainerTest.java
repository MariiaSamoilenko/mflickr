package org.samoilenko.mf;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.boot.test.SpringApplicationConfiguration;

import static org.junit.Assert.*;

/**
 * Test for {@link MainContainer} class.
 */
@SpringApplicationConfiguration(classes = MainContainer.class)
public class MainContainerTest {
    @Ignore
    @Test(expected = Exception.class)
    public void ApplicationTest() {
        MainContainer.main(new String[]{"--spring.output.ansi.enabled=always"});
    }
}
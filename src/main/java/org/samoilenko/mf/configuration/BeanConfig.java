package org.samoilenko.mf.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Configuration class for SpringBootApplication beans.
 */
@Configuration
@EnableWebMvc
@PropertySource({"classpath:/application.properties"})
@ComponentScan({
        "com.samoilenko.mf.domain",
        "com.samoilenko.mf.exceptions",
        "com.samoilenko.mf.controller"
})
public class BeanConfig { }

package org.samoilenko.mf.controller;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * Class UploadFileResponse is designed to implement {@link FileUploadController} response.
 */
public class UploadFileResponse {

    private String imageId;
    private String message;
    private String status;

    /**
     * Default constructor.
     */
    UploadFileResponse() { }

    /**
     * Return the value of {@code status}.
     * @return The value of exchange rate.
     */
    @JsonGetter("status")
    public String getStatus() {
        return status;
    }
    /**
     * To set the {@code status}.
     * @param statusHttp HttpStatus.
     */
    @JsonSetter("status")
    public void setStatus(String statusHttp) {
        this.status = statusHttp;
    }
    /**
     * Return the imageId as {@code String}.
     *
     * @return The message.
     */
    @JsonGetter("imageId")
    public String getImageId() {
        return imageId;
    }
    /**
     * To set the imageId.
     * @param id the id of image..
     */
    @JsonSetter("message")
    public void setImageId(String id) {
        imageId = id;
    }
    /**
     * Return the message.
     *
     * @return The message.
     */
    @JsonGetter("message")
    public String getMessage() {
        return message;
    }
    /**
     * To set the message.
     * @param text The message.
     */
    @JsonSetter("message")
    public void setMessage(String text) {
        message = text;
    }
}

package org.samoilenko.mf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URISyntaxException;

/**
 * This class is responsible for selecting a view to be rendered.
 */
@Controller
public class SiteController {
        /**
         * This field represents the environment in which application is running.
         */
        @Autowired
        private Environment env;
        /**
         * This method mapped on {@code url} "/" .
         * @param model uses build model data with UI
         * @return {@code String} which maps to the file index.html
         */
        @RequestMapping("/")
        public String index(ModelMap model) {
            try {

                model.addAttribute("image_store", this.getClass().getResource("/static/images/").toURI().getPath());
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            model.addAttribute("image_url", env.getProperty("image.url"));
            model.addAttribute("image_description", env.getProperty("image.description"));
            model.addAttribute("image_view", env.getProperty("image.view"));

            return "index";
        }
    }


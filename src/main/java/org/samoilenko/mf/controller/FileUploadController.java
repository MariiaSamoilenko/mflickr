package org.samoilenko.mf.controller;

import org.samoilenko.mf.dao.ImageDao;
import org.samoilenko.mf.domain.Image;
import org.samoilenko.mf.entity.ImageEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * FileUploadController class is responsible for handling requests to upload files.
 * Also this class is responsible for handling requests to get all metadata about stored images.
 */
@Controller
public class FileUploadController {

    @Autowired
    private ImageDao imageDao;
    @Autowired
    private UploadUtil uploadUtil;

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadController.class);

    /**
     * Method handleFileUpload is designed for handling the request toupload files.
     * It maps on /upload url.
     *
     * @param description the description of current file.
     * @param date        the date of creation of this file.
     * @param file        the file which is needed to be uploaded.
     * @return {code String} result of uploading.
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<UploadFileResponse> handleFileUpload(
            @RequestParam("file") MultipartFile file,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam("date") String date) {
        UploadFileResponse response = new UploadFileResponse();
        long imageId;
        if ((!file.isEmpty()) & (uploadUtil.isImage(file.getOriginalFilename()))) {
            try {
                Image image = new Image(description, uploadUtil.verifyDate(date));
                imageId = imageDao.uploadImage(image);
                String filename = uploadUtil.writeFiles(file, imageId, image);
                response.setImageId(filename);
                response.setMessage("The image successfully uploaded");
                response.setStatus(HttpStatus.OK.toString());
                return new ResponseEntity(response, HttpStatus.OK);
            } catch (Exception e) {
                LOGGER.error("Upload error", e);
                response.setImageId("");
                response.setMessage("Upload error=" + e.getMessage());
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
                return new ResponseEntity(response, HttpStatus.OK);
            }
        } else {
            response.setImageId("");
            response.setMessage("Upload error:verify file's extension,please, or it might be empty.");
            response.setStatus(HttpStatus.BAD_REQUEST.toString());
            return new ResponseEntity(response, HttpStatus.OK);
        }
    }

    /**
     * The method to handle /getAll request.
     * @param httpResponse is HttpResponse.
     * @return the{@code List} of Image objects.
     */
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @ResponseBody
    public List<ImageEntity> getAll(HttpServletResponse httpResponse) {
        try {
            List<ImageEntity> list = imageDao.getAll();
            httpResponse.setStatus(HttpStatus.OK.value());
            return list;
        } catch (Exception e) {
            LOGGER.error("Error while getAll method", e);
            httpResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }

    /**
     * Method handles getById requests-to get metadata about image from db by its id.
     * @param id the id to look for image.
     * @param httpResponse is the httpResponse.
     * @return the imageEntity with expected {@code id}.
     * Return empty imageEntity if it doesn't exists.
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ImageEntity getById(@PathVariable("id") long id, HttpServletResponse httpResponse) {
        ImageEntity imageEntity;
        try {
             imageEntity = imageDao.getImageById(id);
            httpResponse.setStatus(HttpStatus.OK.value());
            return imageEntity;
        } catch (Exception e) {
            LOGGER.error("Error while getById method", e);
            httpResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }

    }
}

package org.samoilenko.mf.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.samoilenko.mf.domain.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The class UploadUtil is util class to write file and its details in file system.
 * Files  are stored in predefined directory and named by their ids in database.
 * Details about files are stored in memory db hsqldb.
 * And also to store the short description about files which are were uploaded,
 * application uses json format files, to  populate the db if application was shutdown.
 * This case caused by usage of in-memory db.
 */
@Component("uploadUtil")
public  class UploadUtil {


    private static  final String IMAGE_PATTERN = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadController.class);

    /**
     * Default constructor.
     */
    public UploadUtil() { }

    /**
     * The method writeFiles  writes file in the specified directory and writes it's description.
     * in other predefined directory.
     *
     * @param file    file which is uploaded.
     * @param imageId imageId in database.
     * @param image   im
     * @return the new unique filename which is used in MimicFlickr application.
     * @throws IOException if directory to write not found.
     * @throws URISyntaxException ex.
     */
    public  String writeFiles(MultipartFile file, long imageId, Image image)
            throws IOException, URISyntaxException {
        String filename = null;
        try {
            filename = imageToStore(file, imageId);
        } catch (URISyntaxException e) {
           LOGGER.error("Unable to store file with id=" + imageId, e);
        }
        imageToJson(imageId, image);
        return filename;
    }

    /**
     * The method stores uploaded files in file system.
     *
     * @param file    the file which is need to be stored.
     * @param imageId the Id of image.
     * @return the filename in file system.
     * @throws IOException if directory not found.
     * @throws URISyntaxException ex.
     */
    public  String imageToStore(MultipartFile file, long imageId) throws IOException, URISyntaxException {
        String filename = new Long(imageId).toString();
        String imagePath = this.getClass().getResource("/static/images/").toURI().getPath();
        BufferedOutputStream stream =
                new BufferedOutputStream(new FileOutputStream(new File(new File(imagePath), filename)));
        stream.write(file.getBytes());
        stream.close();
        return filename;
    }

    /**
     * The method stored image's details in {@code json} format files.
     *
     * @param imageId id of image which was already stored.
     * @param image   the image to serialize.
     * @throws IOException if directory not found.
     * @throws URISyntaxException ex.
     */
    public  void imageToJson(long imageId, Image image) throws IOException, URISyntaxException {
        String descriptionPath = this.getClass().getResource("/static/description/").toURI().getPath();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(
                new FileOutputStream(descriptionPath + imageId + ".json"), image);
    }
    /**
     * The method to prevent uploading others files formats except the images.
     *
     * @param filename the {@code} file name.
     * @return true if file is image and false if not.
     */
    public  boolean isImage(String filename) {
        Pattern pattern = Pattern.compile(IMAGE_PATTERN);
        Matcher matcher = pattern.matcher(filename);
        return matcher.matches();
    }

    /**
     * The method to verify is date parameter corerrct.
     * @param date the date.
     * @return {@code String} representation of date in format:YY-MM-DD.
     * @throws DateTimeParseException if date doesn't match.
     */
    public String verifyDate(String date) throws DateTimeParseException {

        return LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE).toString();

    }
}

package org.samoilenko.mf.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * Entity class for ImageEntity.
 */
@Entity
@Table(name = "image")
public class ImageEntity implements Serializable {

    @Id
    @Column(name = "id_image")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idImage;

    @Column(name = "image_data")
    private String dataImage;

    @Column(name = "image_description")
    private String imageDescription;

    /**
     * Default constructor.
     */
    public ImageEntity() {
    }
    /**
     * Constructor with arguments.
     * @param imageDescriptionVal the description of image.
     * @param dataImageVal        the date of image.
     */
    public ImageEntity(String imageDescriptionVal, String dataImageVal) {
        imageDescription = imageDescriptionVal;
        dataImage = dataImageVal;
    }
    /**
     * Get the ID of image.
     *
     * @return ID of image.
     */
    public long getIdImageEntity() {
        return idImage;
    }

    /**
     * To set ID of transaction.
     *
     * @param idImageValue ID of image.
     */
    public void setIdImage(long idImageValue) {
        this.idImage = idImageValue;
    }

    /**
     * Get the Data Image.
     *
     * @return The Data Image.
     */
   public String getDataImage() {
        return dataImage;
    }

    /**
     * To set data of image.
     *
     * @param dataImageValue Data of image.
     */
    public void setDateImage(String dataImageValue) {
        this.dataImage = dataImageValue;
    }

    /**
     * Get the Image description.
     *
     * @return The imageDescription.
     */
    public String getImageDescription() {
        return imageDescription;
    }

    /**
     * To set Image description.
     *
     * @param imageDescriptionValue Quote Currency.
     */
    public void setImageDescription(String imageDescriptionValue) {
        this.imageDescription = imageDescriptionValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ImageEntity that = (ImageEntity) o;

        if (idImage != that.idImage) {
            return false;
        }
        if (!dataImage.equals(that.dataImage)) {
            return false;
        }
        return imageDescription != null ?
                imageDescription.equals(that.imageDescription) : that.imageDescription == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (idImage ^ (idImage >>> 32));
        result = 31 * result + dataImage.hashCode();
        result = 31 * result + (imageDescription != null ? imageDescription.hashCode() : 0);
        return result;
    }
}

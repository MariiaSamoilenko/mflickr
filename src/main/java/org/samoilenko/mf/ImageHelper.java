package org.samoilenko.mf;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.samoilenko.mf.dao.ImageDao;
import org.samoilenko.mf.domain.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The util class for MimicFlicker project.
 * The needed of such class is caused by the reason of in memorydb hsqldb which
 * is used in this project.
 * It upload th data about images in database
 * if there some images in /resources/static/images
 * and there are some descriptions about in /resources/static/description.
 * The description of image stored in ../description folder in json format.
 */

@Component("imageHelper")
public class ImageHelper {
    @Autowired
    private ImageDao imageDao;
    @Autowired
    private Environment env;
        private static final String JSON_PATTERN = "([^\\s]+(\\.(?i)(json))$)";
        private static final Logger LOGGER = LoggerFactory.getLogger(ImageHelper.class);
    /**
     * @return the {@code List} of images data..
     */
    private  List<Image> iterateDescription()  {
        ObjectMapper mapper = new ObjectMapper();
        Image image;
        List<Image> images = new ArrayList<>();
        File f;
        File[] files;
        try {
            f = new File(this.getClass().getResource("/static/description/").getPath());
            files = f.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isFile() && isJson(files[i].getName())) {
                    image = mapper.readValue(files[i], Image.class);
                    images.add(image);
                }
            }
        } catch (Exception e) {
           LOGGER.error("Directory doesn't exists", e);
        }
        return images;
    }
    /**
     *
     * @param list the {@code List} of data about images whic is need to be loaded.
     */
    private  void loadExsistsImagesData(List<Image> list) {
        if (!list.isEmpty()) {
            for (Image aList : list) {
                imageDao.uploadImage(aList);
            }
        }
    }
    /**
     *  The method to load images if there in ../images  dir.
     */
    public  void loadImageData() {
        List<Image> list = iterateDescription();
        loadExsistsImagesData(list);
    }
    private boolean isJson(String filename) {
        Pattern pattern = Pattern.compile(JSON_PATTERN);
        Matcher matcher = pattern.matcher(filename);
        return matcher.matches();
    }
}

package org.samoilenko.mf;

import org.samoilenko.mf.configuration.BeanConfig;
import org.samoilenko.mf.configuration.JpaConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

/**
 * Application com.samoilenko.mf.MainContainer.
 */
@SpringBootApplication
@Import({BeanConfig.class, JpaConfig.class})
@PropertySource({"classpath:/application.properties"})
public class MainContainer extends SpringBootServletInitializer {

    /**
     * The main method.
     * @param args The list of argument.
     */
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication();
        application.setWebEnvironment(true);
        ApplicationContext ctx = application.run(MainContainer.class, args);
        /**
         * Bean {@code helperImage} to load data about images in db, if they are
         * already exists in specified directory.
         */
        ImageHelper hp = ctx.getBean(ImageHelper.class);
        hp.loadImageData();
    }
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MainContainer.class);
    }
}

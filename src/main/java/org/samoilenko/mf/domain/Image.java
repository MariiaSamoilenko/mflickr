package org.samoilenko.mf.domain;

import org.springframework.stereotype.Component;

/**
 * The image pojo class.
 */
@Component("image")
public class Image {

    private String imageDescription;


    private String imageData;

    /**
     * Constructor with parameters.
     * @param description the description of file to uoload.
     * @param data        the date  of current image.
     */
    public Image(String description, String data) {
        imageDescription = description;
        imageData = data;
    }
    /**
     * Default constructor.
     */
    public Image() { }

    /**
     * The getter for image description.
     *
     * @return the description of image.
     */
    public String getImageDescription() {
        return imageDescription;
    }

    /**
     * The setter for image description.
     *
     * @param imageDescription the description which will be settled.
     */
    public void setImageDescription(String imageDescription) {
        this.imageDescription = imageDescription;
    }

    /**
     * The getter for image data.
     *
     * @return the image data.
     */
    public String getImageData() {
        return imageData;
    }

    /**
     * the setter for image data.
     *
     * @param imageData data  whic is need to be settled.
     */
    public void setImageData(String imageData) {
        this.imageData = imageData;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Image image = (Image) o;
        if (!imageDescription.equals(image.imageDescription)) {
            return false;
        }
        return imageData.equals(image.imageData);

    }

    @Override
    public int hashCode() {
        int result = imageDescription.hashCode();
        result =  result + imageData.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Image{" +
                "imageDescription='" + imageDescription + '\'' +
                ", imageData='" + imageData + '\'' +
                '}';
    }
}

package org.samoilenko.mf.dao;

import org.samoilenko.mf.domain.Image;
import org.samoilenko.mf.entity.ImageEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * The class imageDaoImpl is responsible for implementation of dao layer.
 */
@Transactional
@Repository("imageDao")
public class ImageDaoImpl implements ImageDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public long uploadImage(Image image) {
        ImageEntity entity = new ImageEntity();
        entity.setImageDescription(image.getImageDescription());
        entity.setDateImage(image.getImageData());
        em.persist(entity);
        return entity.getIdImageEntity();
    }

    @Override
    public List<ImageEntity> getAll() {
        return  em.createQuery("SELECT e FROM ImageEntity e", ImageEntity.class).getResultList();

    }

    @Override
    public ImageEntity getImageById(long id) {
        TypedQuery<ImageEntity> query = em.createQuery(
                "SELECT e FROM ImageEntity e WHERE e.idImage = :id", ImageEntity.class);
        return query.setParameter("id", id).getSingleResult();
    }
}

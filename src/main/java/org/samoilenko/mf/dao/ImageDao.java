package org.samoilenko.mf.dao;

import org.samoilenko.mf.domain.Image;
import org.samoilenko.mf.entity.ImageEntity;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * The interface of dao for save information on conversion.
 */
@Service
public interface ImageDao {
    /**
     * Save information about uploded image.
     *
     * @param image Information on an image.
     * @return The id of saved image.
     */
    long uploadImage(Image image);

    /**
     * @return the list of uploaded images.
     */
    List<ImageEntity> getAll();

    /**
     *
     * Return the image by its Id.
     * @param id  the id of needed image.
     * @return the required image object.
     */
    ImageEntity getImageById(long id);
}
